"""Manipulations with collected data"""
import pandas as pd


def filter_compatible(results: dict) -> dict:
    return {name: entry
            for name, entry in results.items()
            if not entry.get('error', False)}


def select_summary(compatible: dict) -> pd.DataFrame:
    data = [(name,
             entry['matches_count'],
             entry['good_ratio'],
             entry['total_time']
             ) for name, entry in compatible.items()]

    return pd.DataFrame(data, columns=['Algorithm', 'Matches count',
                                       'Good matches ratio', 'Total time'])


def select_detailed(compatible: dict) -> pd.DataFrame:
    data = [(name,
             *entry['detection_time'],
             *entry['keypoint_count'],
             *entry['description_time'],
             entry['matching_time'],
             entry['matches_count'],
             entry['good_matches_count'],
             entry['good_ratio'],
             entry['total_time']
             ) for name, entry in compatible.items()]

    return pd.DataFrame(data, columns=['Algorithm', 'I1 det. time, s',
                                       'I2 det. time, s',
                                       'I1 KP found', 'I2 KP found',
                                       'I1 ext. time, s', 'I2 ext. time, s',
                                       'Mat. time, s', 'Matches',
                                       'Good matches', 'Good ratio',
                                       'Total time'])


def select_metrics(metrics: dict) -> dict:

    res = {}

    for key in metrics:
        frame = pd.DataFrame(metrics[key])
        frame.set_index('ratio', inplace=True)

        res[key] = frame

    return res
